package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
	private int rows;
	private int cols;
	private CellState initialState;
	private CellState[][] grid;
	

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows=rows;
		this.cols = columns;
		grid = new CellState[rows][cols];
		for(int r=0; r<rows; r++) {
			for (int c=0; c<cols; c++) {
				set(r, c, initialState);
			}
		}
			
	}

    @Override
    public int numRows() {
    	return rows;
       
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
    	if (row<0 || row>= numRows()) {
    		throw new IndexOutOfBoundsException("Illegal row value");
    	}
    	if (column<0 || column >=numColumns()) {
    		throw new IndexOutOfBoundsException("Illegal column value");
    	}
    	grid[row][column]= element;
    	
     
        
    }

    @Override
    public CellState get(int row, int column) {
    	if (row<0 || row>= numRows()) {
    		throw new IndexOutOfBoundsException("Illegal row value");
    	}
    	if (column<0 || column >=numColumns()) {
    		throw new IndexOutOfBoundsException("Illegal column value");
    	}
     
    	return grid[row][column];
    }

    @Override
    public IGrid copy() {
       //CellGrid copy = new CellGrid (rows, cols, initialState);
         //copy.grid= grid;
    	
    	CellGrid copy = new CellGrid (rows, cols, initialState);
    	for(int r=0; r<rows; r++) {
    		for(int c=0; c<cols; c++) {
    			CellState state =this.get(r,c);
    			copy.set(r, c, state);
    		
    		}
    	}
    	
      		
     
        return copy;
    }
    
}
